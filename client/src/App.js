import './App.css';
import { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';

import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import IsLoggedIn from "./auth/AuthGuard";

const firebaseConfig = {
  apiKey: "AIzaSyDjfyYj6A2cB5pGrekCxTFjQfQJBLkjIAA",
  authDomain: "clicker-tanner.firebaseapp.com",
  databaseURL: "https://clicker-tanner-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "clicker-tanner",
  storageBucket: "clicker-tanner.appspot.com",
  messagingSenderId: "99313898302",
  appId: "1:99313898302:web:a9c52b8d1c8d5e139a126f",
  measurementId: "G-BBN8HBX95S"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const LoginPage = lazy(() => import('./pages/LoginPage'));
const ChatPage = lazy(() => import('./pages/ChatPage'));
const HomePage = lazy(() => import('./pages/HomePage'));

function App() {
  const isAuthenticated = IsLoggedIn();

  const navigate = (pageTag) => {
    return isAuthenticated ? pageTag : <Navigate to="/" />
  }

  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path="/" element={isAuthenticated ? <Navigate to="/home" /> : <LoginPage {...{ auth: auth }} />} />
          <Route path="/home" element={navigate(<HomePage {...{ auth: auth }} />)} />
          <Route path="/chat" element={navigate(<ChatPage {...{ auth: auth }} />)} />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
