import React from "react";

// Import our custom hook here again
import usePersistedState from "../custom/PersistedState";

// Import signOut from Firebase Auth
import { signOut } from "firebase/auth";


const HomePage = (props) => {
    const [userLoggedIn, setUserLoggedIn] = usePersistedState("user", {});

    // Add a const to hold value from props.auth
    const auth = props.auth;

    // A Firebase function to handle Sign out
    const handleSignOut = () => {
        signOut(auth)
          .then(() => {
            alert("You are now signed out");
            // Clearing the persisted state and local storage
            setUserLoggedIn({});
            // Redirecting the user back to Auth page
            window.location.pathname = "/";
          })
          .catch((error) => {
            alert("An error ocurred");
            console.log(error);
          });
      };

    const navigateToChat = () => {
      window.location.pathname = "/chat"
    }

    return (
        <div>
            <h2>Home Page</h2>
            <button onClick={handleSignOut}>Sign out!</button>
            <p>I have to be protected</p>
            <button onClick={navigateToChat}>Chat</button>
        </div>
    )
}

export default HomePage;