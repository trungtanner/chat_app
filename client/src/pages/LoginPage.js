import React from "react";

import LoginForm from "../components/LoginForm";

// Accept the prop
const LoginPage = (props) => {
  return (
    <>
      <LoginForm auth={props.auth} />
    </>
  );
};

export default LoginPage;