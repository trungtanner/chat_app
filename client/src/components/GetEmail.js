import React, { useState } from "react";
import axios from 'axios';

function GetEmail() {
    const baseURL = "https://us-central1-clicker-tanner.cloudfunctions.net/register/"

    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");

    const getEmail = async () => {
        await axios.get(`https://us-central1-clicker-tanner.cloudfunctions.net/register/${uid}`)
          .then(response => {
            console.log(response);
            setEmail(response.data.email)
          })
          .catch(error => {
            console.log(error);
          })
      }
    
    return (
        <div>
        <input type="text" placeholder="uid..." onChange={(event) => { setUid(event.target.value) }} />
        <button onClick={getEmail}>&#9658;</button>
        <div>
          <h1>{email}</h1>
        </div>
      </div>
    )
}

export default GetEmail