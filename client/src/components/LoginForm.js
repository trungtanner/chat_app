import React, { Component, useState } from "react";

// Import custom hook
import usePersistedState from '../custom/PersistedState';
// Import signInWithEmailAndPassword
import { signInWithEmailAndPassword } from "@firebase/auth";

class FluidInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            value: ""
        };
    }
    focusField() {
        const { focused } = this.state;
        this.setState({
            focused: !focused
        });
    }
    
    handleChange(event) {
        const { target } = event;
        const { value } = target;
        this.setState({
            value: value
        });

        this.props.onChange(event);
    }
    render() {
        const { type, label, style, id } = this.props;
        const { focused, value } = this.state;

        let inputClass = "fluid-input";
        if (focused) {
            inputClass += " fluid-input--focus";
        } else if (value !== "") {
            inputClass += " fluid-input--open";
        }

        return (
            <div className={inputClass} style={style}>
                <div className="fluid-input-holder">

                    <input
                        className="fluid-input-input"
                        type={type}
                        id={id}
                        onFocus={this.focusField.bind(this)}
                        onBlur={this.focusField.bind(this)}
                        onChange={this.handleChange.bind(this)}
                        autoComplete="off"
                    />
                    <label className="fluid-input-label">{label}</label>

                </div>
            </div>
        );
    }
}

class Button extends Component {
    render() {
        return (
            <div className={`button ${this.props.buttonClass}`} onClick={this.props.onClick}>
                {this.props.buttonText}
            </div>
        );
    }
}

const LoginForm = (props) => {

    const style = {
        margin: "15px 0"
    };

    // Initialize custom hook
    const [userLoggedIn, setUserLoggedIn] = usePersistedState("user", {});
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const auth = props.auth;

    const handleSubmit = () => {
        // const enteredEmail = document.getElementById('email').value;
        // const enteredPassword = document.getElementById('password').value;

        // TODO: do something with the values

        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                const user = userCredential.user;
                setUserLoggedIn(user);
                alert("Logged in successfully");
                // Navigate to home after login
                window.location.pathname = "/home";
            })
            .catch((error) => {
                const errorCode = error.code;
                alert(errorCode);
                console.log(errorCode);
            });
    };

    return (
        <div className="login-container">
            <div className="title">
                Login
            </div>

            <FluidInput type="email" label="email" id="email" style={style} onChange={(event) => { setEmail(event.target.value) }}/>
            <FluidInput type="password" label="password" id="password" style={style} onChange={(event) => { setPassword(event.target.value) }}/>
            <Button buttonText="Log in" buttonClass="login-button" onClick={handleSubmit} />

        </div>
    );
}
export default LoginForm